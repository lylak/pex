$(document).ready( function() {
    $(window).on({
        scroll: function() {
            if($(window).width() >= 768){
                var $srt = $(window).scrollTop();
                if($srt < 120) {
                    var $alpha =  120/10000*$srt <= 0.95 ? 120/10000*$srt : 0.95;
                    $('.pex-header').height(100 - $srt/3).css({'background-color':'rgba(58,49,69,'+$alpha+')'});
                } else {
                    $('.pex-header').height(60).css({'background-color':'rgba(58,49,69,0.95)'});
                }
            }
        },
        load: function() {
            stn_1_h();
        },
        resize: function() {
            stn_1_h();
        }
    });
    stn_1_h();
});

function stn_1_h() {
    $('.section.stn-1').outerHeight($(window).height());
}

/*$(document).ready(function(){
    $('.ph-mobile-btn').click(function(){
        $(this).toggleClass('open');
    });
});*/